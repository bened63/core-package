# Core Package

A Unity project has been created to develop a core package that provides a range of useful scripts, applicable to a wide variety of Unity projects. These scripts are designed to improve the efficiency of development and streamline the process of creating games or applications in Unity. 

## Git Submodules
- [Utilities](https://gitlab.com/bened63/utilities)

## Managers
 - GameManager
 - AudioManager
 - UIManager
