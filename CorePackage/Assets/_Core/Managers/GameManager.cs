using System;
using UnityEngine;

namespace beneder.utilities
{
	public class GameManager : MonoBehaviour
	{
		#region Singleton

		public static GameManager instance;

		private void Awake()
		{
			if (instance != null)
			{
				Destroy(gameObject);
			}
			else
			{
				instance = this;
				DontDestroyOnLoad(gameObject);
			}
		}

		#endregion Singleton
		
		[SerializeField] private bool startLevelOnAwake;

		private bool _isPaused;

		public bool isPaused
		{
			get => _isPaused;
			set
			{
				_isPaused = value;
				if(_isPaused) onGamePaused?.Invoke();
				else onGameResumed?.Invoke();

			}
		}

		public event Action onGamePaused, onGameResumed;

		public event Action onGameStarted;

		private void Start()
		{
			if (startLevelOnAwake)
			{
				StartGame();
			}
		}

		public void StartGame()
		{
		}
	}
}