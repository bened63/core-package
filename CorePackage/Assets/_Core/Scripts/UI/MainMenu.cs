using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class MainMenu : MonoBehaviour
{
	private CanvasGroup _canvasGroup;

	private void Start()
	{
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	private void Update()
	{
	}

	public void Show()
	{
		_canvasGroup.alpha = 1f;
	}

	public void Hide()
	{
		_canvasGroup.alpha = 0f;
	}
}