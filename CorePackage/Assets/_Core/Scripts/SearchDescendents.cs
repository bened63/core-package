using System.Collections;
using System.Collections.Generic;
using beneder.utilities.Audio;
using beneder.utilities.Extensions;
using UnityEngine;

public class SearchDescendents : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var cs = transform.GetComponentsInAllDescendants<Transform>();

        Debug.Log($"cs.Length={cs.Length}");

        for (int i = 0; i < cs.Length; i++)
        {
            Debug.Log($"i={i}, name={cs[i].name}");
        }
        
        AudioManager.instance.Play("blanks");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
