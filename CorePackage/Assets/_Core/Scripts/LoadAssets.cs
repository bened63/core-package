using UnityEngine;
using beneder.utilities;

public class LoadAssets : MonoBehaviour
{
    public string prefabName = "";
    public string assetName = "";
    private AddressableAssetsLoader assetsLoader;

    private void Start()
    {
        assetsLoader = new AddressableAssetsLoader();

        // Example 1: Load and Instantiate a Prefab
        // string prefabAddress = assetName; // Replace with your actual address
        assetsLoader.LoadAndInstantiate(prefabName, OnPrefabLoaded);

        // Example 2: Load a Prefab Async
        // AsyncOperationHandle<GameObject> asyncHandle = assetsLoader.LoadPrefabAsync(assetName);
        // asyncHandle.Completed += OnPrefabLoadedAsync;

        // Example 3: Preload a Prefab
        assetsLoader.Load<ColorData>(assetName, OnAssetPreloaded);
    }

    private void OnPrefabLoaded(GameObject obj)
    {
        Debug.Log($"<color=lime>OnPrefabLoaded</color>");
        obj.transform.position = new Vector3(0, 1, 0);
    }

    private void OnAssetPreloaded(object obj)
    {
        Debug.Log($"<color=lime>OnAssetPreloaded</color>");
        ColorData color = (ColorData)obj;
        Debug.Log($"<color=lime>{obj}</color>");
    }
}